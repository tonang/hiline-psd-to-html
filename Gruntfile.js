/* ------------------------------------------------------------------------- *
 * GLOBALS MODULE, REQUIRE
/* ------------------------------------------------------------------------- */

module.exports = function(grunt) {

	"use strict";

  
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),


    /* UGLIFY CONFIGURATION
    /* ------------------------------------ */
	uglify: {
		global: {
			files: {
				"js/script.min.js": ["js/scripts.js"]
			}
		}
	},

	/* AUTOPREFIXER CONFIGURATION
	/* ------------------------------------ */
	autoprefixer:{
		global: {
			src: "css/app-unprefixed.css",
			dest: "css/app.css"
		}
	}, 

	/* SASS CONFIGURATION
	/* ------------------------------------ */
    sass: {
    	global: {
    		options: {
    			style: "nested"
    		},
    		files: {
    			"css/app-unprefixed.css": "sass/application.scss"
    		}
    	}
    },

    /* JSHINT CONFIGURATION
    /* ------------------------------------ */
    jshint: {
    	options: {
    		force: true
    	},
    	all: ['Gruntfile.js', 'js/scripts.js'],
    },



    /*  ASSEMBLE CONFIGURATION
    **  data: ['template/data/*.{json,yml}']
    /* ------------------------------------ */
    assemble: {
        options:{
            flatten: true,
            published: false,
            partials: ['template/**/*.hbs'],
            layout: ['template/layouts/default.hbs'],
        },
        pages: {
            files: {
                "./": ['template/pages/*.hbs']    
            }
        },
        index: {
            files: {
                "./": ['template/pages/index.hbs']
            }
        }
    },

    /* PRETTIFY CONFIGURATION
    /* ------------------------------------ */
    prettify: {
        options: {
            indent: 4,
            wrap_line_length: 80,
            brace_style: 'expand',
        },
        all: {
            files: [
                {
                    expand: true,
                    src: ['*.html'],
                    ext: '.html'
                }
            ]
        }
    },

    watch: {
		options: {
			livereload: true,
		},

    	gruntfile: {
    		files: 'Gruntfile.js',
    		tasks: ['jshint:gruntfile'],
    	},

    	scripts: {
    		files: ['js/scripts.js'],
    		tasks: ['jshint', 'uglify'],
    	},

    	css: {
    		files: '**/*.scss',
    		tasks: ['sass', 'autoprefixer'],
    	}, // css

        assemble: {
            files: ['template/{,*/}*.hbs' , 'template/data/{,*}*.yml'],
            tasks: ['assemble', 'prettify:all'],
        }, // assemble

    } // watch

  });

  // Load the plugin[s] that provides task.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('assemble');
  grunt.loadNpmTasks('grunt-newer');
  grunt.loadNpmTasks('grunt-prettify');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default task(s).
  grunt.registerTask( 'default', [ 'uglify', 'jshint', 'sass', 'autoprefixer',, 'newer:assemble', 'prettify' ] );

  // Server task(s)
  grunt.registerTask( 'serve', [ 'watch'] );

};