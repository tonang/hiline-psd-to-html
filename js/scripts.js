(function ($) {

	'use strict';

	// NAV-HEADER
	$('.nav-header ul').superfish({
		delay: 400,
		animation: {
			opacity: 'show',
			height: 'show'
		},
		animationOut: {
			opacity: 'hide',
			height: 'hide'
		},
		speed: 200,
		speedOut: 200,
		autoArrows: false
	});

	// HOMEPAGE-CAROUSEL
	// $('.homepage-carousel').owlCarousel({
	// 	animateOut: 'fadeOut',
	// 	items: 1,
	// 	smartSpeed: 100,
	// 	autoplay: false,
	// 	autoplayTimeout: 3000,
	// 	loop: true,
	// });

	// QUOTE-CAROUSEL
	// $('.quote-carousel').owlCarousel({
	// 	animateOut: 'fadeOut',
	// 	items: 1,
	// 	smartSpeed: 100,
	// 	autoplay: false,
	// 	autoplayTimeout: 2000,
	// 	loop: true,
	// });

	// CONFIG ISOTOPE
	// var $container = $(".ins-container");
	// $container.imagesLoaded( function() {
	// 	$container.isotope();
	// });

	// $(".ins-filter a").click( function() {
	// 	var selector = $(this).attr("data-filter");
	// 	$container.isotope({
	// 		itemSelector: ".item.ins",
	// 		filter: selector
	// 	});
	// 	return false;
	// });

	// $(".ins-filter a").click( function (e) {
	// 	$(".ins-filter a").removeClass("active");
	// 	$(this).addClass("active");
	// });

	// NAVIGATION HANDLER
	$(".nav-header > ul").clone(false).find("ul,li").removeAttr("id").remove(".submenu").appendTo($(".nav-rwd-sidebar > ul"));

	$(".btn-rwd-sidebar, .btn-hide").click( function(e) {
		var sidebar_wrapper = $(".nav-rwd-sidebar");
		var main_wrapper = $(".wrapper-inner");
		
		sidebar_wrapper.toggleClass("sidebar-active");
		main_wrapper.toggleClass("wrapper-active");
	})


})(jQuery);